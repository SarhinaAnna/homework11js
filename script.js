"use strict";
let icon = document.querySelectorAll(".icon-password");

for (let i = 0; i < icon.length; i++) {
  icon[i].addEventListener("click", () => {
    if (
      icon[i].parentNode.querySelector(`input`).getAttribute("type") ===
      "password"
    ) {
      icon[i].parentNode.querySelector(`input`).setAttribute("type", "text");
    } else {
      icon[i].parentNode
        .querySelector(`input`)
        .setAttribute("type", "password");
    }
    icon[i].classList.toggle("fa-eye");
    icon[i].classList.toggle("fa-eye-slash");
  });
}

document.querySelector(`.password-form`).addEventListener("submit", (event) => {
  event.preventDefault();
  if (
    document.querySelector(`.password`).value ===
    document.querySelector(`.repeatpassword`).value
  ) {
    alert("You are welcome");
  } else {
    let parag = document.createElement(`p`);
    parag.textContent = `Потрібно ввести однакові значення`;
    document.querySelector(`.repeatpassword`).after(parag);
    parag.style.color = "red";
  }
});
